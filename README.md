# OpenML dataset: Crimes-in-Los-angeles-from-2010

https://www.openml.org/d/43579

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset reflects incidents of crime in the City of Los Angeles dating back to 2010. This data is transcribed from original crime reports that are typed on paper and therefore there may be some inaccuracies within the data. Some location fields with missing data are noted as (0, 0). Address fields are only provided to the nearest hundred block in order to maintain privacy. This data is as accurate as the data in the database.
This data was taken from data.gov(https://catalog.data.gov/dataset/crime-data-from-2010-to-present)
It includes Date and time when was the crime took place, area where it has happen, type of crime and weapon used.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43579) of an [OpenML dataset](https://www.openml.org/d/43579). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43579/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43579/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43579/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

